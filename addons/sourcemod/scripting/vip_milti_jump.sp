#pragma semicolon 1
#include <noCTcfg>
#include <sdktools_functions>
#include <vip_core>
#include <clientprefs>
#include <devzones>
#include <t2lrcfg>
#include <shop>

#define VERSION		"1.0.12"
#define CATEGORY	"stuff"

public Plugin myinfo =
{
	name		= "[VIP] MULTI JUMP",
	author		= "Pheonix (˙·٠●Феникс●٠·˙)",
	version		= VERSION,
	url			= "http://zizt.ru/ http://hlmod.ru/"
};

// SHOP
bool g_bBHop[MAXPLAYERS+1];
ConVar g_hPrice;
ConVar g_hSellPrice;
ConVar sv_autobunnyhopping	= null;
ItemId g_id;

// VIP
int g_iJUMP[MAXPLAYERS+1][5];
float JUMPS[MAXPLAYERS+1];
Handle Cookie;

public OnPluginStart()
{
	Cookie = RegClientCookie("MULTI_JUMP", "MULTI_JUMP", CookieAccess_Protected);
	sv_autobunnyhopping = FindConVar("sv_autobunnyhopping");

	// SHOP	
	g_hPrice = CreateConVar("sm_shop_bhop_price", "50000", "Стоимость покупки BHop.");
	HookConVarChange(g_hPrice, OnConVarChange);
	
	g_hSellPrice = CreateConVar("sm_shop_bhop_sellprice", "40000", "Стоимость продажи BHop.");
	HookConVarChange(g_hPrice, OnConVarChange);

	AutoExecConfig(true, "shop_bhop", "shop");
	
	if (Shop_IsStarted()) Shop_Started();
}

public OnConVarChange(ConVar hCvar, const char[] oldValue, const char[] newValue)
{
	if(g_id != INVALID_ITEM) {
		if(hCvar == g_hPrice) Shop_SetItemPrice(g_id, GetConVarInt(hCvar));
		else if(hCvar == g_hSellPrice) Shop_SetItemSellPrice(g_id, GetConVarInt(hCvar));
	}
}

public OnPluginEnd() Shop_UnregisterMe();

public void Shop_Started()
{
	CategoryId category_id = Shop_RegisterCategory(CATEGORY, "Дополнительно", "");
	
	if (Shop_StartItem(category_id, "bhop")) {
		Shop_SetInfo("[CT-Only] BunnyHop", "Зажмите пробел и прыгайте (только для КТ)", GetConVarInt(g_hPrice), GetConVarInt(g_hSellPrice), Item_Togglable, -1);
		Shop_SetCallbacks(OnItemRegistered, OnItemUsed);
		Shop_EndItem();
	}
}

public void OnItemRegistered(CategoryId category_id, const char[] category, const char[] item, ItemId item_id)
{
	g_id = item_id;
}

public ShopAction OnItemUsed(int iClient, CategoryId category_id, const char[] category, ItemId item_id, const char[] item, bool isOn, bool elapsed)
{
	if (isOn || elapsed) {
		g_bBHop[iClient] = false;
		return Shop_UseOff;
	}

	g_bBHop[iClient] = true;

	return Shop_UseOn;
}

public void OnClientPostAdminCheck(int iClient) 
{
	g_bBHop[iClient] = false;
}

//public void OnMapStart()
//{
	//KeyValues kv = new KeyValues("MULTI_JUMP");
	//char path[128];
	//BuildPath(Path_SM, path, 128, "data/vip/modules/multi_jamp.ini");
	//if(!kv.ImportFromFile(path))
	//	SetFailState("[MULTI JUMP] - Файл конфигураций не найден");
	//else {
	//	PJUMP = kv.GetNum("JUMP");							// стоит 0
	//	if(PJUMP == 3)
	//		PJUMPS = kv.GetFloat("JUMP_K");					// стоит ""
	//	else if(PJUMP == 2)
	//		PJUMPD = kv.GetNum("JUMP_K");
	//}
//}

public int VIP_OnVIPLoaded()
{
	VIP_RegisterFeature("MULTI_JUMP", STRING, SELECTABLE, SELECTABLE_ITEM, OnDisplayItem, ItemDraw);
}

public int VIP_OnVIPClientLoaded(int iClient)
{
	char buf[3][10];
	VIP_GetClientFeatureString(iClient, "MULTI_JUMP", buf[0], 10);
	ExplodeString(buf[0], ";", buf, 3, 10);
	StringToIntEx(buf[0], g_iJUMP[iClient][0]);		// доступен ли баннихоп
	StringToIntEx(buf[1], g_iJUMP[iClient][1]);		// доступны ли двойные и т.д прыжки (указать количество прыжков)
	StringToFloatEx(buf[2], JUMPS[iClient]);		// cупер прыжок (высоту (Число с точкой))
	if(JUMPS[iClient] > 0.0)
		g_iJUMP[iClient][2] = 1;						// доступен ли супер прыжок
	GetClientCookie(iClient, Cookie, buf[0], 10);
	int b = StringToInt(buf[0]);
	
	if(b && g_iJUMP[iClient][b-1])
		g_iJUMP[iClient][4] = b;
	else g_iJUMP[iClient][4] = 0;
}

public void OnClientPutInServer(int iClient)
{
	for (int i; i < 5; ++i)
		g_iJUMP[iClient][i] = 0;
}

public int ItemDraw(int iClient, const char[] sFeatureName, int iStyle)
{
	if(g_iJUMP[iClient][0] || g_iJUMP[iClient][1] || g_iJUMP[iClient][2])
		return iStyle;
	return ITEMDRAW_DISABLED;
}

public bool SELECTABLE_ITEM(int iClient, const char[] sSubMenuName)
{
	int buf = g_iJUMP[iClient][4];
	for (;;) {
		buf++;
		if(buf > 3) {
			g_iJUMP[iClient][4] = 0;
			break;
		} else if(g_iJUMP[iClient][buf-1]) {
			g_iJUMP[iClient][4] = buf;
			break;
		}
	}
	save(iClient);
	return true;
}

void save(int iClient)
{
	char buf[5];
	IntToString(g_iJUMP[iClient][4], buf, 5);
	SetClientCookie(iClient, Cookie, buf);
} 

public bool OnDisplayItem(int iClient, const char[] sFeatureName, char[] sDisplay, int maxlen)
{
	switch (g_iJUMP[iClient][4]) {
		case 0: strcopy(sDisplay, maxlen, "Прыжки [Стандарт]");
		case 1: strcopy(sDisplay, maxlen, "Прыжки [BunnyHop]");
		case 2: FormatEx(sDisplay, maxlen, "Прыжки [%d прыжка(ов)]", g_iJUMP[iClient][1]);
		case 3: strcopy(sDisplay, maxlen, "Прыжки [Супер прыжок]");
	}
	return true;
}

public Action OnPlayerRunCmd(int iClient, int &fCurButtons, int &impulse, float vel[3], float angles[3], int &weapon)
{
	if (!GetConVarBool(sv_autobunnyhopping) && IsPlayerAlive(iClient)) {
		//static int fCurFlags;
		//static bool bSp[MAXPLAYERS+1];
		//fCurFlags = GetEntityFlags(iClient);
		if (fCurButtons & IN_JUMP && (g_bBHop[iClient] && GetClientTeam(iClient) == 3 || g_iJUMP[iClient][4]==1 || Zone_IsClientInZone(iClient, "BHop")) ) {
			if (GetEntProp(iClient, Prop_Data, "m_nWaterLevel") <= 1 && !(GetEntityMoveType(iClient) & MOVETYPE_LADDER)) {
				SetEntPropFloat(iClient, Prop_Send, "m_flStamina", 0.0);
				if (!(GetEntityFlags(iClient) & FL_ONGROUND))
					fCurButtons &= ~IN_JUMP;
			}
		} //else if(g_iJUMP[iClient][4] == 2) {
		//	static int u;
		//	u = g_iJUMP[iClient][1];
		//	if (g_fLastFlags[iClient] & FL_ONGROUND && !(fCurFlags & FL_ONGROUND) && !(g_fLastButtons[iClient] & IN_JUMP) && fCurButtons & IN_JUMP)
		//		g_iJumps[iClient]++;
		//	else if(fCurFlags & FL_ONGROUND)
		//		g_iJumps[iClient] = 1;
		//	else if(!(g_fLastButtons[iClient] & IN_JUMP) && fCurButtons & IN_JUMP && g_iJumps[iClient] < u) {
		//		g_iJumps[iClient]++;
		//		float vVel[3];
		//		GetEntPropVector(iClient, Prop_Data, "m_vecVelocity", vVel);
		//		vVel[2] = 300.0;
		//		TeleportEntity(iClient, NULL_VECTOR, NULL_VECTOR, vVel);
		//	}
		//} else if(iClient && g_iJUMP[iClient][4] == 3) {
		//	if(!(g_fLastButtons[iClient] & IN_JUMP) && fCurButtons & IN_JUMP && !bSp[iClient]) {
		//		bSp[iClient] = true;
		//		float vVel[3];
		//		GetEntPropVector(iClient, Prop_Data, "m_vecVelocity", vVel);
		//		vVel[2] = 300.0*JUMPS[iClient];
		//		TeleportEntity(iClient, NULL_VECTOR, NULL_VECTOR, vVel);
		//	} else if(fCurFlags & FL_ONGROUND)
		//		bSp[iClient] = false;
		//}
		//g_fLastFlags[iClient] = fCurFlags;
		//g_fLastButtons[iClient] = fCurButtons;
	}
	return Plugin_Continue;
}